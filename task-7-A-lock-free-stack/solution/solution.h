#pragma once

#include <atomic>
#include <thread>
#include <vector>

///////////////////////////////////////////////////////////////////////

template <typename T>
class LockFreeStack {
    struct Node {
        T element;
        Node* next = nullptr;

        Node(T elem) : element(elem) {};
    };

 public:
    explicit LockFreeStack() {
    }

    ~LockFreeStack() {
        Node* curr = top_;
        while (curr) {
            Node* next = curr->next;
            delete curr;
            curr = next;
        }

        for (std::size_t i = 0; i < index_; ++i) {
            delete trash_[i];
        }
    }

    void Push(T element) {
        Node* new_top = new Node(element);

        Node* curr_top = top_.load(std::memory_order_acquire);
        new_top->next = curr_top;

        while (!top_.compare_exchange_strong(curr_top, new_top,
                                             std::memory_order_acq_rel)) {
            new_top->next = curr_top;
        }
    }

    bool Pop(T& element) {
        Node* curr_top = top_.load(std::memory_order_acquire);

        while (true) {
            if (!curr_top) {
                return false;
            }
            if (top_.compare_exchange_strong(curr_top, curr_top->next,
                                             std::memory_order_acq_rel)) {
                element = curr_top->element;
                size_t index = index_.fetch_add(1, std::memory_order_acq_rel);
                trash_[index] = curr_top;
                return true;
            }
        }
    }

 private:
    std::atomic<Node*> top_{nullptr};
    std::array<Node*, 100000> trash_;
    std::atomic<std::size_t> index_{0};
};

///////////////////////////////////////////////////////////////////////

template <typename T>
using ConcurrentStack = LockFreeStack<T>;

///////////////////////////////////////////////////////////////////////
