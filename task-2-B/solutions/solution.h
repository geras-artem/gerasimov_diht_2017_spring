#pragma once

#include <cstddef>
#include <iostream>
#include <mutex>
#include <condition_variable>

//Решение через cond-var'ы. Закоментил, чтобы заслать в контест решение через семафоры.

//class Robot {
//public:
//    void StepLeft() {
//        std::unique_lock<std::mutex> lock(mtx_);
//        while (step) {
//            cv_.wait(lock);
//        }
//
//        std::cout << "left" << std::endl;
//        step = !step;
//        cv_.notify_one();
//    }
//
//    void StepRight() {
//        std::unique_lock<std::mutex> lock(mtx_);
//        while (!step) {
//            cv_.wait(lock);
//        }
//
//        std::cout << "right" << std::endl;
//        step = !step;
//        cv_.notify_one();
//    }
//
//private:
//    bool step = false;
//    std::mutex mtx_;
//    std::condition_variable cv_;
//};

class Sem {
public:
    Sem(std::size_t threads_num = 1, std::size_t size = 0)
    {
        size_ = size;
        capacity_ = threads_num;
    }

    void increase()
    {
        std::unique_lock<std::mutex> lock(mtx_);
        while (size_ == capacity_) {
            inc_cv_.wait(lock);
        }

        ++size_;
        dec_cv_.notify_one();
    }

    void decrease()
    {
        std::unique_lock<std::mutex> lock(mtx_);
        while (size_ == 0) {
            dec_cv_.wait(lock);
        }

        --size_;
        inc_cv_.notify_one();
    }

private:
    std::mutex mtx_;
    std::condition_variable inc_cv_;
    std::condition_variable dec_cv_;
    std::size_t capacity_;
    std::size_t size_;
};

class Robot {
public:
    Robot() :
            sem1_(1, 1),
            sem2_(1, 0) {};

    void StepLeft() {
        sem1_.decrease();

        std::cout << "left" << std::endl;

        sem2_.increase();
    }

    void StepRight() {
        sem2_.decrease();

        std::cout << "right" << std::endl;

        sem1_.increase();
    }

private:
    Sem sem1_, sem2_;
};