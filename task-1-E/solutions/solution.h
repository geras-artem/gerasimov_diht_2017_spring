#pragma once

#include <cstddef>
#include <atomic>
#include <thread>

class PetersonMutex {
private:
    std::atomic<std::size_t> victim_;
    std::array<std::atomic<bool>, 2> want_;

public:
    PetersonMutex();
    void lock(const std::size_t t);
    void unlock(const std::size_t t);
};

PetersonMutex::PetersonMutex() {
    want_[0].store(false);
    want_[1].store(false);
    victim_ = 0;
}

void PetersonMutex::lock(std::size_t t) {
    want_[t].store(true);
    victim_.store(t);
    while (want_[1 - t] && victim_ == t) {
        std::this_thread::yield();
    }
}
void PetersonMutex::unlock(std::size_t t) {
    want_[t].store(false);
}


class TreeMutex {
private:
    std::size_t tree_size_;
    std::unique_ptr<PetersonMutex[]> tree_;

public:
    TreeMutex(std::size_t num_threads);

    void lock(std::size_t current_thread);

    void unlock(std::size_t current_thread);
};

TreeMutex::TreeMutex(std::size_t num_threads) {
    std::size_t power = 1;
    while (power * 2 < num_threads) {
        power *= 2;
    }
    tree_size_ = power * 2 - 1;
    tree_ = std::unique_ptr<PetersonMutex[]>(new PetersonMutex[tree_size_]);
}

void TreeMutex::lock(std::size_t current_thread) {
    for (std::size_t index = tree_size_ + current_thread;
            index != 0; index = (index - 1) / 2) {
        tree_[(index - 1) / 2].lock((index - 1) % 2);
    }
}

void TreeMutex::unlock(std::size_t current_thread) {
    std::size_t capacity = (tree_size_ + 1) / 2;
    std::size_t mutex_index = 0;
    std::size_t thread_index = current_thread;
    while (mutex_index < tree_size_) {
        tree_[mutex_index].unlock(thread_index / capacity);
        if (thread_index / capacity) {
            mutex_index = mutex_index * 2 + 2;
            thread_index -= capacity;
            capacity /= 2;
        } else {
            mutex_index = mutex_index * 2 + 1;
            capacity /= 2;
        }
    }
}