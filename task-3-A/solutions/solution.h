#include <iostream>
#include <condition_variable>
#include <queue>
#include <mutex>
#include <stdexcept>

class QueueException : public std::exception {
    virtual const char* what() const noexcept
    {
        return "queue is shutdown";
    }
};

template <class T, class Container = std::deque<T>>
class BlockingQueue {
public:
    explicit BlockingQueue(const size_t& capacity)
    {
        capacity_ = capacity;
        is_off_ = false;
    }

    // You need to implement Put method which will block and wait while capacity
    // of your container exceeded. It should work with non-copyable objects and
    // use move semantics for placing elements in the container. If the queue is
    // shutdown you need to throw an exception inherited from std::exception.
    void Put(T&& el)
    {
        std::unique_lock<std::mutex> lock(mtx_);

        if (is_off_) {
            throw QueueException();
        }

        while (queue_.size() == capacity_ && !is_off_) {
            put_cv_.wait(lock);
        }

        if (is_off_) {
            throw QueueException();
        }

        queue_.push_back(std::move(el));

        if (queue_.size() == 1) {
            get_cv_.notify_all();
        }
    }

    // Get should block and wait while the container is empty, if the queue
    // shutdown you will need to return false.
    bool Get(T& result)
    {
        std::unique_lock<std::mutex> lock(mtx_);

        if (is_off_) {
            return false;
        }

        while (queue_.empty() && !is_off_) {
            get_cv_.wait(lock);
        }

        if (is_off_) {
            return false;
        }

        result = std::move(queue_.front());
        queue_.pop_front();

        if (queue_.size() == capacity_ - 1) {
            put_cv_.notify_all();
        }

        return true;
    }

    // Shutdown should turn off the queue and notify every thread which is waiting
    // for something to stop wait.
    void Shutdown()
    {
        std::unique_lock<std::mutex> lock(mtx_);
        is_off_ = true;
        put_cv_.notify_all();
        get_cv_.notify_all();
    }

private:
    Container queue_;
    std::size_t capacity_;
    bool is_off_;
    std::mutex mtx_;
    std::condition_variable put_cv_;
    std::condition_variable get_cv_;
};
