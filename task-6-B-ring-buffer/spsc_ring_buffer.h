#pragma once

#include <atomic>
#include <vector>

//////////////////////////////////////////////////////////////////////

// Single-Producer/Single-Consumer Fixed-Size Ring Buffer (Queue)

template <typename T>
class SPSCRingBuffer {
public:
    explicit SPSCRingBuffer(const std::size_t capacity)
        : buffer_(capacity + 1) {
    }

    bool Publish(T element) {
        const std::size_t curr_head = head_.load(std::memory_order_acquire);
        const std::size_t curr_tail = tail_.load(std::memory_order_relaxed);

        if (Full(curr_head, curr_tail)) {
            return false;
        }

        buffer_[curr_tail] = element;
        tail_.store(Next(curr_tail), std::memory_order_release);
        return true;
    }

    bool Consume(T& element) {
        const std::size_t curr_head = head_.load(std::memory_order_relaxed);
        const std::size_t curr_tail = tail_.load(std::memory_order_acquire);

        if (Empty(curr_head, curr_tail)) {
            return false;
        }

        element = buffer_[curr_head];
        head_.store(Next(curr_head), std::memory_order_release);
        return true;
    }

private:
    bool Full(const std::size_t head, const std::size_t tail) const {
        return Next(tail) == head;
    }

    bool Empty(const std::size_t head, const std::size_t tail) const {
        return tail == head;
    }

    std::size_t Next(const std::size_t slot) const {
        return (slot + 1) % buffer_.size();
    }

private:
    std::vector<T> buffer_;
    std::atomic<std::size_t> tail_{0};
    std::atomic<std::size_t> head_{0};
};

//////////////////////////////////////////////////////////////////////
