#pragma once

#include <cstddef>
#include <iostream>
#include <mutex>
#include <condition_variable>
#include <vector>

class Sem {
public:
    Sem(std::size_t threads_num = 1, std::size_t size = 0)
    {
        size_ = size;
        capacity_ = threads_num;
    }

    void setSize(std::size_t size)
    {
        size_ = size;
    }

    void increase()
    {
        std::unique_lock<std::mutex> lock(mtx_);
        while (size_ == capacity_) {
            inc_cv_.wait(lock);
        }

        ++size_;
        dec_cv_.notify_one();
    }

    void decrease()
    {
        std::unique_lock<std::mutex> lock(mtx_);
        while (size_ == 0) {
            dec_cv_.wait(lock);
        }

        --size_;
        inc_cv_.notify_one();
    }

private:
    std::mutex mtx_;
    std::condition_variable inc_cv_;
    std::condition_variable dec_cv_;
    std::size_t capacity_;
    std::size_t size_;
};

class Robot {
public:
    Robot(const std::size_t num_foots) :
            sem_(num_foots)
    {
        sem_[0].setSize(1);
        num_foots_ = num_foots;
    }

    void Step(const std::size_t foot)
    {
        sem_[foot].decrease();

        std::cout << "foot " << foot << std::endl;

        sem_[(foot + 1) % num_foots_].increase();
    }

private:
    std::vector<Sem> sem_;
    std::size_t num_foots_;
};