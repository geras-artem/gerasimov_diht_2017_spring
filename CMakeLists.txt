cmake_minimum_required(VERSION 3.6)
project(gerasimov_diht_2017_spring)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")

add_subdirectory(task-1-E)
add_subdirectory(task-3-B)