**Очередь**

```cpp
void Queue::Initialize() {
     dummy = new node();
     head = tail = dummy;
 }
 
void Queue::Enqueue(T item) {
     new_tail = new Node(item);
     prev_tail = tail.exchange(new_tail); **(1)**
     prev_tail->next = new_tail;
 }
 
bool Queue::Dequeue(T& item) {
    if (!head->next) {
        return false;
    }
    item = head->next->item;
    head = head->next;
    return true;
}
```

Лианиризуема ли она?

**Решение:**

Я осознал свою ошибку и теперь заявляю: она не линеаризуема. Приведём демонстрирующую это историю.

|T1|T2|T3|
|:---:|:---:|:---:|
|начало Enq(x)| | |
| | Enq(y)| |
| | | Deq()|
|конец Exq(x)| | |

Пусть у нас изначально пустая очередь.
Пусть **T1** Начнёт выполнять `Enq(x)` и остановится, послу вполнения строчки **(1)**. Таким образом,
**T2** добавит *y* после *x*, но **T3** посмотрит на `head->next` и, увидев там **nullptr**, вернёт **false**.
Это линеаризуется только в историю, где `Deq()` происходит раньше обоих добавлений, но это противоречит порядку
данной нам истории.
