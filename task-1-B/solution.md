**B. Tricky Mutex**

Рассмотрим реализацию мьютекса на RMW-операциях инкремента и декремента:

class tricky_mutex {

public:

   void lock() {
   
       while (thread_count.fetch_add(1) > 0) {
       
           thread_count.fetch_sub(1);
           
       }
       
   }
   

   void unlock() {
   
       thread_count.fetch_sub(1);
       
   }

private:

   std::atomic_int thread_count = 0;
   
};

Гарантирует ли эта реализация взаимное исключение (mutual exclusion) и свободу от взаимной блокировки (deadlock freedom)?

**Решение:**
1) Рассмотрим взаимное исключение. При захвате потоком мьютекса счётчик увеличивается с 0 на единицу.
Это значит, что следующий поток при попытке захватить мьютекс зациклится, так как счётчик не сможет уменьшиться до 0,
пока поток, захвативший мьютекс не уменьшит его, вызвав unlock().
2) Рассмотрим возможность deadlock-а. Допустим, что deadlock возможен. Тогда два потока оказываются замкнутыми в циклах

while (thread_count.fetch_add(1) > 0) {
           thread_count.fetch_sub(1);
       }

Но заметим, что при любых их действиях значения count меняются в пределах от 0 до 2.
При этом если значение равно 1, то оно увеличено одним потоком, а значит, что он может сразу его уменьшить,
зайти в цикл ещё раз со значением count = 0 и получить доступ в критическую секцию.
Если count = 0, то любой тот поток, который первый увеличит счётчик и получит мьютекс.
Тогда остаётся только случай, когда count = 2. Это значит, что оба потока находятся внутри цикла перед выполнением уменьшения.
Но тогда они оба по очереди могут выполнить его, почле чего один из них сможет получить доступ к мьютексу.
Таким образом из любого состояния есть выход, а значит и deadlock невозможен.

Аналогично рассматривается ситуация с n потоками. Пусть счётчик равен k. Это значение было получено увеличениями из k потоков.
Тогда если они по очереди выполнят уменьшения, то мы получим count = 0, что значит, что первый поток, когторый выполнит увеличение,
получит контроль над мьютексом. Опять есть выход.

Получили результат: deadlock невозможен.

Важно не спутать deadlock и livelock, так как второй в этой ситуации возможен:

| Поток 0                                                                                                                                             | Поток 1                                                                                                                  |
|-----------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------|
| while (thread_count.fetch_add(1) > 0) {      thread_count.fetch_sub(1); } //Поток увеличит count на 1 и пройдёт //в критическую секцию. (count = 1) |                                                                                                                          |
|                                                                                                                                                     | while (thread_count.fetch_add(1) > 0) { //Поток увеличит count, но не будет допущен в  //критическую секцию. (count = 2) |
| unlock(); //(count = 1)                                                                                                                             |                                                                                                                          |
| while(thread_count.fetch_add(1) > 0) { //Поток увеличит count, но также не пройдёт //в критическую секцию. (count = 2)                              |                                                                                                                          |

Очевидно, что для этой ситуации существует последовательность операций, при выполнении которой, потоки не выйдут из цикла. Например, только один поток будет работать, или
они будут по очереди отрабатывать по такту цикла.