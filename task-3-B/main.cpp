#include "solutions/solution.h"
#include <chrono>

int main() {
    ThreadPool<size_t> pool(1);
    std::vector<std::future<size_t>> future;

    for (size_t i = 0; i < 1; ++i) {
        future.push_back(std::move(pool.Submit({[i]() {
            std::this_thread::sleep_for(std::chrono::seconds(i % 10));
            return i;
        }})));
    }

    for (auto &f : future) {
        std::cout << f.get() <<std::endl;
    }

    return 0;
}