#pragma once

#include <iostream>
#include <condition_variable>
#include <queue>
#include <mutex>
#include <stdexcept>
#include <functional>
#include <future>
#include <atomic>

class QueueException : public std::exception {
    virtual const char* what() const noexcept
    {
        return "queue is shutdown";
    }
};

class PoolException : public std::exception {
    virtual const char* what() const noexcept
    {
        return "pool is shutdown";
    }
};

template <class T, class Container = std::deque<T>>
class BlockingQueue {
public:
    explicit BlockingQueue()
    {
        is_off_ = false;
    }

    void Put(T&& el)
    {
        std::unique_lock<std::mutex> lock(mtx_);

        if (is_off_) {
            throw QueueException();
        }

        queue_.push_back(std::move(el));

        if (queue_.size() == 1) {
            get_cv_.notify_all();
        }
    }

    bool Get(T& result)
    {
        std::unique_lock<std::mutex> lock(mtx_);

        while (queue_.empty() && !is_off_) {
            get_cv_.wait(lock);
        }

        if (is_off_ && queue_.empty()) {
            return false;
        }

        result = std::move(queue_.front());
        queue_.pop_front();

        return true;
    }

    void Shutdown()
    {
        std::unique_lock<std::mutex> lock(mtx_);
        is_off_ = true;
        get_cv_.notify_all();
    }

    bool empty()
    {
        std::unique_lock<std::mutex> lock(mtx_);
        return queue_.empty();
    }

private:
    Container queue_;
    bool is_off_;
    std::mutex mtx_;
    std::condition_variable get_cv_;
};

template <class T>
class ThreadPool {
public:
    explicit ThreadPool(const std::size_t num_threads = DefaultNumWorkers())
    {
        in_action_.store(true);
        for (std::size_t i = 0; i < num_threads; ++i) {
            pool_.emplace_back([this]() {
                std::packaged_task<T()> task;
                while (in_action_.load() || !queue_.empty()) {
                    if (queue_.Get(task))
                        task();
                }
            });
        }

    }

    static std::size_t DefaultNumWorkers()
    {
        std::size_t res = std::thread::hardware_concurrency();
        if (res)
            return res;
        else
            return 4;
    }

    std::future<T> Submit(std::function<T()> task)
    {
        if (!in_action_.load()) {
            throw PoolException();
        }
        std::packaged_task<T()> tsk(task);
        auto future = tsk.get_future();
        queue_.Put(std::move(tsk));
        return future;
    }

    void Shutdown()
    {
        in_action_.store(false);
        queue_.Shutdown();
        for (auto &thread : pool_) {
            thread.join();
        }
    }

    ~ThreadPool()
    {
        if (in_action_.load())
            Shutdown();
    }

private:
    std::atomic_bool in_action_;
    BlockingQueue<std::packaged_task<T()>> queue_;
    std::vector<std::thread> pool_;
};
