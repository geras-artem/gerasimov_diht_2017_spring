**C. Метод try_lock для ticket спинлока**

Семантика метода try_lock: захватить мьютекс без ожидания (если это возможно) и вернуть true; если попытка захвата не удалась, и потоку нужно ждать, то вернуть false.

Неудачный вызов try_lock не должен влиять на другие потоки (например, блокировать их).

Придумайте реализацию метода try_lock для ticket спинлока. Методы lock и unlock должны остаться без изменений.

В качестве разогревающего упражнения - придумайте реализацию try_lock для test-and-set спинлока.

**Решение:**

1) test-and-set спинлок

try_lock() {

    return !locked.exchange(true);
    
}

2) ticket спинлок

try_lock() {
    
    temp = owner_ticket.load();
    return next_ticket.compare_exchange(temp, temp + 1);
    
}