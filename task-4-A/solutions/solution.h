#pragma once

#include <functional>
#include <vector>
#include <forward_list>
#include <mutex>
#include <condition_variable>
#include <algorithm>
#include <atomic>

class RWLock {
public:
    RWLock() :
    readers_counter_(0),
    writers_counter_(0),
    is_being_writen_(false) {};

    RWLock(const RWLock&) = delete;

    void ReadLock()
    {
        std::unique_lock<std::mutex> lock(mutex_);

        while (writers_counter_ > 0) {
            cv_.wait(lock);
        }

        ++readers_counter_;
    }

    void WriteLock()
    {
        std::unique_lock<std::mutex> lock(mutex_);

        ++writers_counter_;

        while (readers_counter_ > 0 || is_being_writen_) {
            cv_.wait(lock);
        }

        is_being_writen_ = true;
    }

    void ReadUnlock()
    {
        std::unique_lock<std::mutex> lock(mutex_);

        --readers_counter_;

        if (readers_counter_ == 0) {
            cv_.notify_all();
        }
    }

    void WriteUnlock()
    {
        std::unique_lock<std::mutex> lock(mutex_);

        --writers_counter_;
        is_being_writen_ = false;
        cv_.notify_all();
    }

private:
    std::size_t readers_counter_;
    std::size_t writers_counter_;
    bool is_being_writen_;

    std::mutex mutex_;
    std::condition_variable cv_;
};

class ReadLockGuard{
public:
    ReadLockGuard(RWLock& lock) : lock_(lock)
    {
        lock_.ReadLock();
    }

    ~ReadLockGuard()
    {
        lock_.ReadUnlock();
    }

private:
    RWLock& lock_;
};

class WriteLockGuard{
public:
    WriteLockGuard(RWLock& lock) : lock_(lock)
    {
        lock_.WriteLock();
    }

    ~WriteLockGuard()
    {
        lock_.WriteUnlock();
    }

private:
    RWLock& lock_;
};


template <typename T, class Hash = std::hash<T>>
class StripedHashSet {
 public:
    explicit StripedHashSet(const std::size_t concurrency_level,
                            const std::size_t growth_factor = 3,
                            const double load_factor = 0.75) {
        size_.store(0);
        concurrency_level_ = concurrency_level;
        growth_factor_ = growth_factor;
        load_factor_ = load_factor;
        capacity_ = concurrency_level_ * growth_factor_;

        buckets_.resize(capacity_);
        locks_ = std::vector<RWLock>(concurrency_level_);
    }

    bool Insert(const T& element) {
        std::size_t element_hash_value = hash_function_(element);
        std::size_t stripe_index = GetStripeIndex(element_hash_value);
        locks_[stripe_index].WriteLock();

        std::size_t bucket_index = GetBucketIndex(element_hash_value);
        if (buckets_[bucket_index].end() ==
                std::find(buckets_[bucket_index].begin(),
                          buckets_[bucket_index].end(), element)) {
            if (size_ / capacity_ > load_factor_) {
                locks_[stripe_index].WriteUnlock();
                Rehash();
                locks_[stripe_index].WriteLock();
                bucket_index = GetBucketIndex(element_hash_value);
            }
            buckets_[bucket_index].push_front(element);
            size_.fetch_add(1);
            locks_[stripe_index].WriteUnlock();
            return true;
        }
        locks_[stripe_index].WriteUnlock();
        return false;
    }

    bool Remove(const T& element) {
        std::size_t element_hash_value = hash_function_(element);
        std::size_t stripe_index = GetStripeIndex(element_hash_value);
        locks_[stripe_index].WriteLock();

        std::size_t bucket_index = GetBucketIndex(element_hash_value);
        if (buckets_[bucket_index].end() !=
            std::find(buckets_[bucket_index].begin(),
                      buckets_[bucket_index].end(), element)) {
            buckets_[bucket_index].remove(element);
            size_.fetch_sub(1);
            locks_[stripe_index].WriteUnlock();
            return true;
        }
        locks_[stripe_index].WriteUnlock();
        return false;
    }

    bool Contains(const T& element) {
        std::size_t element_hash_value = hash_function_(element);
        std::size_t stripe_index = GetStripeIndex(element_hash_value);
        ReadLockGuard lg((locks_[stripe_index]));

        std::size_t bucket_index = GetBucketIndex(element_hash_value);
        std::forward_list<T>& current_bucket = buckets_[bucket_index];
        return current_bucket.end() != std::find(current_bucket.begin(),
                                                 current_bucket.end(), element);
    }

    std::size_t Size() {
        return size_;
    }

 private:
    std::size_t GetBucketIndex(const std::size_t element_hash_value) const {
        return element_hash_value % capacity_;
    }

    std::size_t GetStripeIndex(const std::size_t element_hash_value) const {
        return element_hash_value % concurrency_level_;
    }

    void Rehash() {
        locks_.begin()->WriteLock();
        if (size_ / capacity_ <= load_factor_) {
            locks_.begin()->WriteUnlock();
            return;
        }

        for (auto it = ++locks_.begin(); it != locks_.end(); ++it) {
            it->WriteLock();
        }

        capacity_ *= growth_factor_;
        std::vector<std::forward_list<T>> new_buckets(capacity_);

        for (auto& bucket : buckets_) {
            for (auto& element : bucket) {
                std::size_t new_bucket_index = GetBucketIndex(hash_function_(element));
                new_buckets[new_bucket_index].push_front(element);
            }
        }

        buckets_.swap(new_buckets);

        for (auto& lock : locks_) {
            lock.WriteUnlock();
        }
    }


 private:
    std::atomic<std::size_t> size_;
    std::size_t capacity_;
    std::size_t concurrency_level_;
    std::size_t growth_factor_;
    double load_factor_;
    Hash hash_function_;

public:
    std::vector<std::forward_list<T>> buckets_;
    std::vector<RWLock> locks_;
};

template <typename T> using ConcurrentSet = StripedHashSet<T>;