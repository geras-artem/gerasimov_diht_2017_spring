**Точка линеаризации для Enqueue в очереди Майкла-Скотта**

Можно ли в качестве точки линеаризации для вызова Enqueue выбрать тот успешный CAS,
который передвигает указатель Tail на вставленный узел? 

Заметим, что его не обязательно выполняет тот же поток, который выполняет Enqueue.

**Решение:**

```cpp
    void Enqueue(T element) {
        counter_.fetch_add(1);
        Node* new_tail = new Node(element);
        Node* curr_tail;

        while (true) {
            curr_tail = tail_.load();
            if (!curr_tail->next_.load()) {
                Node* p = nullptr;
                if (curr_tail->next_.compare_exchange_strong(p, new_tail)) { (1)
                    break;
                }
            } else {
                tail_.compare_exchange_weak(curr_tail, curr_tail->next_.load());
            }
        }

        tail_.compare_exchange_weak(curr_tail, new_tail);
        counter_.fetch_sub(1);
    }
```

Я понял свою ошибку, так что теперь попытаюсь доказать, что он дествительно является
точкой линеаризации.

Итак, в следствие того, сама по себе весьма хороша для линеаризации, покажем, что порядок
элементов в ней будет в точности таким же, как порядок вышеуказанных вызовов.

Во-первых, да, моё предыдущее предположение о том, что эти вызовы не всегда будут внутри
временных границ операции добавления неверно, так как если наш указатель не передвинул другой поток,
то его передвинет сам поток, который вызвал добавление, перед тем, как завершить функцию.

Теперь заметим, если поток добавил элемент в очередь, т.е. выполнил строчку (1) с положительным
результатом, то *tail_* был сдвинут "до упора", перед этим. Далее, ни один поток не
добавит элемент, пока также не указатель на *tail_* не будет подвинут. Таким образом, действительно,
указатель *tail_* сдвигается на только что добавленный элемент перед добавлением нового,
что значит, удачные вызовы CAS-a идут в том же порядке, что элементы в очереди.
Таким образом, их действиельно можно выбрать в качестве точек линеаризации.