#pragma once

#include <condition_variable>
#include <cstddef>
#include <mutex>

template <typename ConditionVariable = std::condition_variable>
class CyclicBarrier {
public:
    CyclicBarrier(std::size_t num_threads)
    {
        enter_counter_ = 0;
        exit_counter_ = 0;
        num_threads_ = num_threads;
    }

    void Pass()
    {
        std::unique_lock<std::mutex> lock(mtx_);


        enter_counter_ = (enter_counter_ + 1) % num_threads_;
        if (!enter_counter_) {
            cv_.notify_all();
        }
        while (enter_counter_) {
            cv_.wait(lock);
        }


        exit_counter_ = (exit_counter_ + 1) % num_threads_;
        if (!exit_counter_) {
            cv_.notify_all();
        }
        while (exit_counter_) {
            cv_.wait(lock);
        }
    }

private:
    std::mutex mtx_;
    ConditionVariable cv_;
    std::size_t num_threads_;
    std::size_t enter_counter_;
    std::size_t exit_counter_;
};