#pragma once

#include <thread>
#include <atomic>

///////////////////////////////////////////////////////////////////////

template <typename T, template <typename U> class Atomic = std::atomic>
class LockFreeQueue {
    struct Node {
        T element_{};
        Atomic<Node*> next_{nullptr};

        explicit Node(T element, Node* next = nullptr)
            : element_(std::move(element))
            , next_(next) {
        }

        explicit Node() {
        }
    };

 public:
    explicit LockFreeQueue() {
        Node* dummy = new Node{};
        head_ = dummy;
        tail_ = dummy;
        trash_head_ = dummy;
    }

    ~LockFreeQueue() {
        counter_.fetch_add(1);
        head_.store(nullptr);
        TryReclaim();
    }

    void Enqueue(T element) {
        counter_.fetch_add(1);
        Node* new_tail = new Node(element);
        Node* curr_tail;

        while (true) {
            curr_tail = tail_.load();
            if (!curr_tail->next_.load()) {
                Node* p = nullptr;
                if (curr_tail->next_.compare_exchange_strong(p, new_tail)) {
                    break;
                }
            } else {
                tail_.compare_exchange_weak(curr_tail, curr_tail->next_.load());
            }
        }

        tail_.compare_exchange_weak(curr_tail, new_tail);
        counter_.fetch_sub(1);
    }

    bool Dequeue(T& element) {
        counter_.fetch_add(1);
        while (true) {
            Node* curr_head = head_.load();
            Node* curr_tail = tail_.load();

            if (curr_head == curr_tail) {
                if (!curr_head->next_.load()) {
                    counter_.fetch_sub(1);
                    return false;
                } else {
                    tail_.compare_exchange_weak(curr_head, curr_head->next_.load());
                }
            } else {
                if (head_.compare_exchange_strong(curr_head, curr_head->next_.load())) {
                    element = curr_head->next_.load()->element_;
                    TryReclaim();
                    return true;
                }
            }
        }
    }

 private:
    void TryReclaim() {
        Node* cur_head = head_.load();
        Node* cur_trash_head = trash_head_.load();

        if (counter_ == 1) {
            while (cur_trash_head != cur_head) {
                Node* next = cur_trash_head->next_.load();
                delete cur_trash_head;
                cur_trash_head = next;
            }
            trash_head_.store(cur_trash_head);
        }

        counter_.fetch_sub(1);
    }

 private:
    std::atomic<std::size_t> counter_{0};
    std::atomic<Node*> trash_head_{nullptr};
    Atomic<Node*> head_{nullptr};
    Atomic<Node*> tail_{nullptr};
};

///////////////////////////////////////////////////////////////////////
